FROM directus/directus

LABEL author="Carlos Carvalho<contato@carlosocarvalho.com.br>"
LABEL image="directus-custom"

WORKDIR /directus/

COPY ./uploads /directus/uploads

COPY ./extensions /directus/extensions

EXPOSE 8055